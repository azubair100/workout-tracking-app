require "rails_helper"

RSpec.feature "Creating Exercise" do

  before do
    @john = User.create!(first_name: 'john', last_name: 'doe', email:'john@example.com', password:'password')
    @jane = User.create!(first_name: 'jane', last_name: 'doe', email:'jane@example.com', password:'password')
    @sara = User.create!(first_name: 'sara', last_name: 'doe', email:'sara@example.com', password:'password')

    @room_name = @john.first_name + '-' + @john.last_name
    @room = Room.create!(name: @room_name, user_id: @john.id)

    # include Warden::Test::Helpers in /spec/rails_helper.rb below the requires
    login_as(@john)

    Friendship.create(user: @sara, friend: @john)
    Friendship.create(user: @jane, friend: @john)


  end

  scenario 'followers are show in the chatroom' do
    visit '/'

    click_link 'My Lounge'

    expect(page).to have_content(@room_name)

    fill_in 'message-field', with: 'hello'
    click_button 'Send'
    expect(page).to have_content('hello')

    within('#followers') do
      expect(page).to have_content(@sara.full_name)
      expect(page).to have_content(@jane.full_name)
    end

  end

end