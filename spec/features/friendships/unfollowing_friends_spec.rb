require "rails_helper"

RSpec.feature "Unfollowing Friends" do

  before do
    @john = User.create!(first_name: 'john', last_name: 'doe', email:'john@example.com', password:'password')
    @jane = User.create!(first_name: 'jane', last_name: 'doe', email:'jane@example.com', password:'password')



    login_as(@john)

    @following = Friendship.create(user: @john, friend: @jane)
  end


  scenario 'Shows friend workout for 7 days' do

    visit '/'

    click_link 'My Lounge'

    link = "a[href ='/friendships/#{@following.id}'][data-method='delete']"
    find(link).click

    expect(page).to have_content(@jane.full_name + ' unfollowed')



  end

end