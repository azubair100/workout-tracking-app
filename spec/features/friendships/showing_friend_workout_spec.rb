require "rails_helper"

RSpec.feature "Showing friends" do

  before do
    @john = User.create!(first_name: 'john', last_name: 'doe', email:'john@example.com', password:'password')
    @jane = User.create!(first_name: 'jane', last_name: 'doe', email:'jane@example.com', password:'password')


    @e1 = @john.exercises.create(
        duration_in_minute: 20,
        workout: 'my bodybuilding activity.',
        workout_date: Date.today
    )
    @e2 = @jane.exercises.create(
        duration_in_minute: 55,
        workout: 'weight lifting',
        workout_date: Date.today
    )
    login_as(@john)

    @following = Friendship.create(user: @john, friend: @jane)
  end


  scenario 'Shows friend workout for 7 days' do

    visit '/'

    click_link 'My Lounge'
    click_link @jane.full_name

    expect(page).to have_content(@e2.workout)
    expect(page).to have_content(@jane.full_name + "'s Exercises")
    expect(page).to have_css("div#chart")



  end

end