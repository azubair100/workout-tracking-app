require "rails_helper"

RSpec.feature "Listing Members" do

  before do
    @john = User.create!(first_name: 'john', last_name: 'doe', email:'john@example.com', password:'password')
    @jane = User.create!(first_name: 'jane', last_name: 'doe', email:'jane@example.com', password:'password')


  end


  scenario 'listing all the registered suers' do

    visit '/'
    expect(page).to have_content('List of Members')
    expect(page).to have_content(@john.full_name)
    expect(page).to have_content(@jane.full_name)

  end

end