require "rails_helper"

RSpec.feature "Creating Exercise" do

  before do
    @john = User.create!(first_name: 'john', last_name: 'doe', email:'john@example.com', password:'password')
    @jane = User.create!(first_name: 'jane', last_name: 'doe', email:'jane@example.com', password:'password')


  end


  scenario 'with existing names returns all users' do

    visit '/'

    fill_in 'search_name', with: 'Doe'
    click_button 'Search'

    expect(page).to have_content(@john.full_name)
    expect(page).to have_content(@jane.full_name)
    expect(current_path).to eq('/dashboards/search')

  end

end