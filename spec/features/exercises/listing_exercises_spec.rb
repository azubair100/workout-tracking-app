require 'rails_helper'

RSpec.feature 'Listing Exercises' do

  before do
    @john = User.create!(first_name: 'john', last_name: 'doe', email:'john@example.com', password:'password')
    @jane = User.create!(first_name: 'jane', last_name: 'doe', email:'jane@example.com', password:'password')
    # include Warden::Test::Helpers in /spec/rails_helper.rb below the requires

    login_as(@john)



    @e1 = @john.exercises.create(
                                  duration_in_minute: 20,
                                  workout: 'whatever whatever whatevr.',
                                  workout_date: Date.today
                                )
    @e2 = @john.exercises.create(
                                  duration_in_minute: 55,
                                  workout: 'weighting bodying lifting creating.',
                                  workout_date: 2.days.ago
                                )
    @e3 = @john.exercises.create(
                                  duration_in_minute: 65,
                                  workout: 'trademil.',
                                  workout_date: 12.days.ago
                                )

    @following = Friendship.create(user: @john, friend: @jane)

  end


  scenario "shows user's workout for last 7 days" do

    visit '/'

    click_link 'My Lounge'
    expect(page).to have_content(@e1.duration_in_minute)
    expect(page).to have_content(@e1.workout)
    expect(page).to have_content(@e1.workout_date)

    expect(page).to have_content(@e2.duration_in_minute)
    expect(page).to have_content(@e2.workout)
    expect(page).to have_content(@e2.workout_date)

    expect(page).not_to have_content(@e3.duration_in_minute)
    expect(page).not_to have_content(@e3.workout)
    expect(page).not_to have_content(@e3.workout_date)

  end

  scenario "shows no exercises if none created" do
    @john.exercises.delete_all

    visit '/'

    click_link 'My Lounge'
    expect(page).to have_content('No Workouts Yet')

  end

  scenario 'shows the list of friends' do
    visit '/'

    click_link 'My Lounge'

    expect(page).to have_content('My Friends')
    expect(page).to have_link(@jane.full_name)
    expect(page).to have_link('Unfollow')
  end

end