require "rails_helper"

RSpec.feature "Creating Exercise" do

  before do
    @john = User.create!(first_name: 'john', last_name: 'doe', email:'john@example.com', password:'password')
    # include Warden::Test::Helpers in /spec/rails_helper.rb below the requires
    login_as(@john)

    visit '/'

    click_link 'My Lounge'
    click_link 'New Workout'

    expect(page).to have_link('Back')
  end

  scenario 'with valid inputs' do


    fill_in 'Duration', with: 70
    fill_in 'Workout details', with: 'Weight Lifting'
    fill_in 'Activity date', with: 2.days.ago

    click_button 'Create Exercise'


    expect(page).to have_content('Exercise has been created')

    exercise = Exercise.last
    expect(current_path).to eq(user_exercise_path(@john, exercise))

    expect(exercise.user_id).to eq(@john.id)
  end


  scenario 'with invalid inputs' do

    fill_in 'Duration', with: ''
    fill_in 'Workout details', with: ''
    fill_in 'Activity date', with: ''

    click_button 'Create Exercise'


    expect(page).to have_content('Exercise has not been created')
    expect(page).to have_content('not a number')
    expect(page).to have_content(' be blank')
    expect(page).to have_content('be blank')


  end
end