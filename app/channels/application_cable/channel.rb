module ApplicationCable
  class Channel < ActionCable::Channel::Base
    identified_by :current_user

    def connect
      self.current_user = find_current_user
    end

    def disconnect

    end

    protected

    def  find_current_user
      if current_user = User.find_by(id: cookies.signed['user_id'])
        current_user
      else
        reject_unauthorized_connection
        # the cookie is not available when dievise create it in this environment
        # you will have to create it, config/initialize
      end
    end

  end
end
