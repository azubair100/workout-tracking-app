This is a workout tracking app.. using rails 5. There are some d3.js in it and implementation of action cable is coming soon. I have followed TDD so I have respect in it.

Some of the features are as follows:

Multiple users login and logout. (using devise)
Each individual user able to see their workouts. 
Follow other users as a friend or unfollow them.